const fs = require("fs");
const path = require("path");
const mongoose = require("mongoose");
const globalConfig = require("../config/config");

const env = process.env.NODE_ENV || "development";
const config = globalConfig.mongoose[env];
// Connect to MongoDB
mongoose
  .connect(
    config.uri,
    { useNewUrlParser: true }
  )
  .then(() => {
    console.log("MongoDB Connected");
  })
  .catch(err => console.log(err));

const basename = path.basename(module.filename);
let _models = {};
const files = fs.readdirSync(__dirname);

files
  .filter(file => {
    return (
      file.indexOf(".") !== 0 &&
      file !== basename &&
      (file.slice(-3) === ".js" || file.slice(-3) === ".ts") &&
      file !== "interfaces"
    );
  })
  .forEach(file => {
    let model = require(path.join(__dirname, file));
    //let model = mongoose.model("User", schema);
    _models[model.modelName] = model;
  });

module.exports = {
  models: _models,
  mongoose
};
