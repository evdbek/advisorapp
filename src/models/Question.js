const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const QuestionSchema = new Schema(
  {
    client: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true
    },
    title: {
      type: String,
      require: true
    },
    body: {
      type: String,
      required: true
    },
    comments: [
      {
        body: {
          type: String,
          required: true
        },
        user: {
          type: Schema.Types.ObjectId,
          ref: "users"
        },
        name: {
          type: String,
          require: true
        },
        avatar: {
          type: String
        }
      },
      {
        timestamps: {
          createdAt: "created_at",
          updatedAt: "updated_at"
        }
      }
    ]
  },
  {
    timestamps: {
      createdAt: "created_at",
      updatedAt: "updated_at"
    }
  }
);

module.exports = Question = mongoose.model("Question", QuestionSchema);
