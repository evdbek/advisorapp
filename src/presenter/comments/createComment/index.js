const catchErrors = require("../../utils/catchErrors");
const { OK } = require("http-status-codes");
const getAuthUser = require("../../../utils/jwt/getAuthUser");
// prettier-ignore
const {  maxLength, minLength, isEnum, validateMatchingPasswords } = require("../../../utils/validate");
// prettier-ignore
const { maybe, required, checkType, restrictToSchema, composeRules} = require("rulr");
// prettier-ignore
const { COMMENTS_ADD_CHANNEL, TEXT_FIELD_LENGTH } = require("../../../utils/constants");

const { CREATED } = require("http-status-codes");

const validateCreateComment = maybe(
  composeRules([
    restrictToSchema({
      body: required(maxLength(TEXT_FIELD_LENGTH), checkType(String)),
      name: required(maxLength(TEXT_FIELD_LENGTH), checkType(String)),
      avatar: required(checkType(String))
    })
  ])
);

module.exports = config => {
  return catchErrors(config, async (req, res) => {
    const { user } = await getAuthUser({ req, service: config.service });

    validateCreateComment(req.body, ["comment"]);
    const { question_id } = req.params;
    const createdComment = await config.service.createComment({
      user,
      question_id,
      ...req.body
    });
    const id = createdComment.user;
    if (config.pusher) {
      config.pusher.trigger(
        user.type === "client"
          ? createdComment.user._id.toString()
          : createdComment.client._id.toString(),
        COMMENTS_ADD_CHANNEL,
        createdComment
      );
    }
    res.status(CREATED).json(createdComment);
  });
};
