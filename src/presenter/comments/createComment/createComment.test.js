const initTests = require("../../utils/initTests");
const {
  API_ROUTE,
  USER_TYPE_CLIENT,
  USER_TYPE_USER
} = require("../../../utils/constants");
const expectError = require("../../utils/expectError");
const generateJwtToken = require("../../../utils/jwt/generateToken");
const createUser = require("../../utils/createUser");
const moment = require("moment");
const { fakeQuestions, fakeComments } = require("../../../utils/fakesFactory");
const {
  CREATED,
  CONFLICT,
  UNAUTHORIZED,
  FORBIDDEN,
  UNPROCESSABLE_ENTITY,
  NOT_FOUND
} = require("http-status-codes");
const {
  TEST_INVALID_JWT_TOKEN,
  TEST_VALID_TITLE,
  TEST_VALID_DESCRIPTION,
  TEST_VALID_REGISTER_USER
} = require("../../../utils/testValues");

describe(__filename, () => {
  const { service, request } = initTests();

  it("should fail to create comment when not provided question id", async () => {
    const response = await request
      .post(`${API_ROUTE}/questions/comments`)
      .send({ question_id: 1 });
    expectError(response, NOT_FOUND);
  });

  it("should fail to create comment when unauthenticated", async () => {
    const question_id = 1;
    const response = await request
      .post(`${API_ROUTE}/questions/comments/` + question_id)
      .send({ question_id: 1 });
    expectError(response, UNAUTHORIZED);
  });

  it("should fail to create comment when invalid token provided in authorization header", async () => {
    const question_id = 1;
    const response = await request
      .post(`${API_ROUTE}/questions/comments/` + question_id)
      .set("Authorization", TEST_INVALID_JWT_TOKEN);
    expectError(response, UNAUTHORIZED);
  });

  it("should fail to create comment for question which does not exist", async () => {
    const user = await createUser(service, USER_TYPE_CLIENT);

    const validToken = await generateJwtToken({ id: user.id });

    const question_id = 999;
    const response = await request
      .post(`${API_ROUTE}/questions/comments/` + question_id)
      .set("Authorization", validToken)
      .send({
        user_id: user.id,
        body: TEST_VALID_DESCRIPTION
      });

    expectError(response, UNPROCESSABLE_ENTITY);
  });

  it("should fail to create comment for question which does not exist", async () => {
    const client = await createUser(service, USER_TYPE_CLIENT);
    const user = await createUser(service, USER_TYPE_USER);
    const postData = fakeQuestions({
      count: 1,
      overrides: { user_id: user.id, user, client },
      only: ["user_id", "title", "body", "user", "client"]
    });
    const createQuestion = await service.createQuestion(postData);

    const validToken = await generateJwtToken({ id: user.id });
    const question_id = createQuestion.id;
    const response = await request
      .post(`${API_ROUTE}/questions/comments/` + question_id)
      .set("Authorization", validToken)
      .send({
        user_id: user.id,
        body: TEST_VALID_DESCRIPTION
      });

    expectError(response, UNPROCESSABLE_ENTITY);
  });

  it("should successfully create comment with valid data", async () => {
    const client = await createUser(service, USER_TYPE_CLIENT);
    const user = await createUser(service, USER_TYPE_USER);

    const postData = fakeQuestions({
      count: 1,
      overrides: { user_id: user.id, user, client },
      only: ["user_id", "title", "body", "user", "client"]
    });
    const createQuestion = await service.createQuestion(postData);

    const validToken = await generateJwtToken({ id: user._id });
    const question_id = createQuestion.id;
    const response = await request
      .post(`${API_ROUTE}/questions/comments/` + question_id)
      .set("Authorization", validToken)
      .send({
        body: TEST_VALID_DESCRIPTION,
        name: user.name,
        avatar: "dummy text"
      });
    const createdQuestionComment = response.body;

    +expect(response.status).toBe(CREATED);
    const now = moment();
    // The post is returned instead of single comment, so updated of post is checked
    const correctUpdatedAt =
      moment.duration(now.diff(response.body.updatedAt)).asMilliseconds() <
      10000;
    expect(createQuestion.user.id).toEqual(user.id);
    expect(createQuestion.client.id).toEqual(client.id);
    expect(createdQuestionComment._id).toEqual(createQuestion.id);
    expect(correctUpdatedAt).toBe(true);
  });
});
