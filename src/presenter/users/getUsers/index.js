const catchErrors = require("../../utils/catchErrors");
const { OK } = require("http-status-codes");
const getAuthUser = require("../../../utils/jwt/getAuthUser");
const { maybe, optional, checkType, restrictToSchema } = require("rulr");
const { isEnum } = require("../../../utils/validate");
const { USER_TYPES } = require("../../../utils/constants");

const validateGetUsers = maybe(
  restrictToSchema({
    type: optional(isEnum(USER_TYPES)),
    limit: optional(checkType(String))
  })
);

module.exports = config => {
  return catchErrors(config, async (req, res) => {
    await getAuthUser({ req, service: config.service });

    validateGetUsers(req.query, ["users"]);

    const { type, limit, offset, sort } = req.query;

    const users = await config.service.getUsers({
      type,
      limit,
      offset,
      order: sort
    });

    res.status(OK).json(users);
  });
};
