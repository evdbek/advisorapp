const getUsers = require("./getUsers");
const getUsersById = require("./getUsersById");

module.exports = {
  getUsers,
  getUsersById
};
