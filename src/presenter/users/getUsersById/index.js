const catchErrors = require("../../utils/catchErrors");
const { OK } = require("http-status-codes");
const getAuthUser = require("../../../utils/jwt/getAuthUser");
const { maybe, optional, checkType, restrictToSchema } = require("rulr");
const { isEnum } = require("../../../utils/validate");
const { USER_TYPES } = require("../../../utils/constants");

const validateGetUsers = maybe(
  restrictToSchema({
    type: optional(isEnum(USER_TYPES)),
    limit: optional(checkType(String))
  })
);

module.exports = config => {
  return catchErrors(config, async (req, res) => {
    await getAuthUser({ req, service: config.service });

    validateGetUsers(req.query, ["users"]);

    const { id } = req.params;

    const users = await config.service.getUsersById({ id });

    res.status(OK).json(users);
  });
};
