const enchanedRouter = require("./enchanedRouter");
const passport = require("passport");
const auth = require("./auth");
const users = require("./users/");
const questions = require("./questions");
const comments = require("./comments");

module.exports = config => {
  const router = enchanedRouter();

  router.post("/auth/login", auth.login(config));
  router.post("/auth/register", auth.register(config));

  router.get("/users/", users.getUsers(config));
  router.get("/users/:user_id", users.getUsersById(config));

  router.post("/questions", questions.createQuestion(config));
  router.get("/questions", questions.getQuestions(config));
  router.get("/question/:question_id", questions.getQuestionById(config));

  router.post(
    "/questions/comments/:question_id",
    comments.createComment(config)
  );

  return router;
};
