const {
  Warning,
  TypeWarning,
  RequiredWarning,
  RestrictedKeysWarning
} = require("rulr");
const {
  MinLengthWarning,
  MaxLengthWarning,
  NotMatchingPasswordWarning,
  InvalidEmailWarning
} = require("../../utils/validate");

module.exports = (messenger, warning) => {
  switch (warning.constructor) {
    case MinLengthWarning:
      return messenger.minLengthWarning(warning);
    case MaxLengthWarning:
      return messenger.minLengthWarning(warning);
    case TypeWarning:
      return messenger.typeWarning(warning);
    case RequiredWarning:
      return messenger.requiredWarning(warning);
    case RestrictedKeysWarning:
      return messenger.restrictedKeysWarning(warning);
    case InvalidEmailWarning:
      return messenger.invalidEmailWarning(warning);
    case NotMatchingPasswordWarning:
      return messenger.notMatchingPasswordWarning(warning);
    default:
      return messenger.warning(warning);
  }
};
