const { UNPROCESSABLE_ENTITY } = require("http-status-codes");

module.exports = (response, expectedStatusCode = UNPROCESSABLE_ENTITY) => {
  expect(response.status).toBe(expectedStatusCode);
  expect(response.body).toMatchSnapshot();
};
