const express = require("express");
const presenter = require("../index");
const createSupertest = require("supertest");
const serviceFactory = require("../../service/");
const messengerFactory = require("../../messenger/");
const { API_ROUTE } = require("../../utils/constants");

const app = express();

const service = serviceFactory();
const messenger = messengerFactory();

const presenterFacade = presenter({
  service,
  messenger
});

app.use(API_ROUTE, presenter({ service, messenger }));

const request = createSupertest(app);
module.exports = () => {
  beforeAll(async () => {
    await service.rollback();
    await service.migrate();
  });

  beforeEach(async () => {
    await service.clearService();
    await service.migrate();
  });

  afterEach(async () => {
    await service.rollback();
    await service.migrate();
  });

  return { service, request };
};
