const Service = require("../../service");
const { fakeUsers } = require("../../utils/fakesFactory");
const faker = require("faker");

module.exports = (service, type) => {
  return new Promise(async (resolve, reject) => {
    const newUser = fakeUsers({
      count: 1,
      only: ["email", "name", "password", "type"],
      overrides: { type: type }
    });
    try {
      const user = await service.createUser(newUser);
      resolve(user);
    } catch (e) {
      console.log(e);
      reject(e);
    }
  });
};
