const handleError = require("./handleError");

module.exports = (config, handler) => {
  return async (req, res) => {
    try {
      await handler(req, res);
    } catch (err) {
      return handleError({ config, res, err });
    }
  };
};
