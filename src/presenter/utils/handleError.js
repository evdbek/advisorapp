const {
  UNPROCESSABLE_ENTITY,
  UNAUTHORIZED,
  INTERNAL_SERVER_ERROR,
  FORBIDDEN,
  NOT_FOUND,
  CONFLICT
} = require("http-status-codes");
const rulr = require("rulr");
const { Warnings } = rulr;
const translateWarnings = require("./translateErrors");
const {
  ForbiddenError,
  UserAlreadyExistsError,
  InvalidCredentialsError,
  NotFoundError,
  ModelNotFoundError,
  UnauthorizedError,
  MissingJwtTokenError,
  ExpiredJwtTokenError,
  InvalidJwtTokenError
} = require("../../utils/errors");

module.exports = ({ config, res, err }) => {
  const { messenger } = config;

  if (err instanceof Warnings) {
    const warnings = err.warnings;
    const errors = {};
    warnings.forEach(warning => {
      errors[warning.path[warning.path.length - 1]] = translateWarnings(
        messenger,
        warning
      );
    });
    /*const errors = warnings.map(warning => {
      return translateWarnings(messenger, warning);
    });*/
    const message = "Unprocessable entity";
    //logError(message);
    return res.status(UNPROCESSABLE_ENTITY).json({ errors, message });
  }

  if (err instanceof InvalidJwtTokenError) {
    const message = messenger.invalidJwtToken();
    //logError(message);
    return res.status(UNAUTHORIZED).json({ message });
  }

  if (err instanceof MissingJwtTokenError) {
    const message = messenger.missingJwtToken();
    // logError(message);
    return res.status(UNAUTHORIZED).json({ message });
  }

  if (err instanceof InvalidCredentialsError) {
    const message = messenger.invalidCredentials();
    //logError(message);
    return res.status(UNPROCESSABLE_ENTITY).json({ message });
  }

  if (err instanceof ModelNotFoundError) {
    const message = messenger.modelNotFound(err.modelName);
    // logError(message);
    return res.status(UNPROCESSABLE_ENTITY).json({ message });
  }

  if (err instanceof UserAlreadyExistsError) {
    const message = messenger.userAlreadyExists();
    // logError(message);
    return res.status(CONFLICT).json({ message });
  }

  const message = messenger.serverError();
  //logError(message);
  console.log("server error", err, err.message);
  return res.status(INTERNAL_SERVER_ERROR).json({ message });
};
