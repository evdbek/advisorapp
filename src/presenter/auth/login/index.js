const catchErrors = require("../../utils/catchErrors");
const {
  Warning,
  maybe,
  required,
  restrictToSchema,
  checkType,
  checkRegex
} = require("rulr");
const { OK } = require("http-status-codes");
const { isEmail } = require("../../../utils/validate");
class InvalidEmailWarning extends Warning {
  constructor(data, path) {
    super(data, path);
  }
}
const validateLogin = maybe(
  restrictToSchema({
    email: required(isEmail),
    password: required(checkType(String))
  })
);

module.exports = config => {
  return catchErrors(config, async (req, res) => {
    validateLogin(req.body, ["user"]);

    const { email, password } = req.body;

    const { user, token } = await config.service.login({
      email,
      password
    });

    res.status(OK).json({ user, token });
  });
};
