const initTests = require("../../utils/initTests");
const { API_ROUTE } = require("../../../utils/constants");
const expectError = require("../../utils/expectError");
const { CREATED, CONFLICT } = require("http-status-codes");
const {
  TEST_VALID_LOGIN_USER,
  TEST_VALID_REGISTER_USER,
  TEST_INVALID_EMAIL,
  TEST_VALID_PASSWORD,
  TEST_VALID_EMAIL,
  TEST_TOO_SHORT_PASSWORD,
  TEST_DIFFERENT_VALID_PASSWORD,
  TEST_VALID_USER_TYPE,
  TEST_VALID_USER_NAME
} = require("../../../utils/testValues");

describe(__filename, () => {
  const { service, request } = initTests();

  it("should fail to register a user without input", async () => {
    const response = await request.post(`${API_ROUTE}/auth/register`);
    expectError(response);
  });

  it("should fail to register a user when email is invalid", async () => {
    const response = await request.post(`${API_ROUTE}/auth/register`).send({
      email: TEST_INVALID_EMAIL,
      password: TEST_VALID_PASSWORD,
      password_confirmation: TEST_VALID_PASSWORD,
      name: TEST_VALID_USER_NAME,
      type: TEST_VALID_USER_TYPE
    });
    expectError(response);
  });

  it("should fail to register a user without password", async () => {
    const response = await request
      .post(`${API_ROUTE}/auth/register`)
      .send({ email: TEST_VALID_EMAIL });
    expectError(response);
  });

  it("should fail to register a user with too short password", async () => {
    const response = await request.post(`${API_ROUTE}/auth/register`).send({
      email: TEST_VALID_EMAIL,
      password: TEST_TOO_SHORT_PASSWORD,
      name: TEST_VALID_USER_NAME,
      type: TEST_VALID_USER_TYPE
    });
    expectError(response);
  });

  it("should fail to register a user with the same email address", async () => {
    const user = await service.register(TEST_VALID_REGISTER_USER);

    const response = await request
      .post(`${API_ROUTE}/auth/register`)
      .send(TEST_VALID_REGISTER_USER);
    expectError(response, CONFLICT);
  });

  it("should fail to register a user when the password does not match password_confirmation", async () => {
    const response = await request.post(`${API_ROUTE}/auth/register`).send({
      email: TEST_VALID_EMAIL,
      password: TEST_VALID_EMAIL,
      password_confirmation: TEST_DIFFERENT_VALID_PASSWORD,
      name: TEST_VALID_USER_NAME,
      type: TEST_VALID_USER_TYPE
    });
    expectError(response);
  });

  it("should succesfully register a user", async () => {
    const response = await request
      .post(`${API_ROUTE}/auth/register`)
      .send(TEST_VALID_REGISTER_USER);
    const { user, token } = response.body;
    expect(response.status).toBe(CREATED);
    expect(user.email).toEqual(TEST_VALID_REGISTER_USER.email);
  });
});
