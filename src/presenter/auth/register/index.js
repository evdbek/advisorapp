// prettier-ignore
const { isEmail, minLength, isEnum, validateMatchingPasswords } = require("../../../utils/validate");
// prettier-ignore
const { maybe, required, restrictToSchema, checkType, composeRules, first } = require("rulr");
const catchErrors = require("../../utils/catchErrors");
const { CREATED } = require("http-status-codes");
// prettier-ignore
const { USER_PASSWORD_MIN_LENGTH, USER_NAME_MIN_LENGTH, USER_TYPES } = require("../../../utils/constants");

const validateRegister = maybe(
  composeRules([
    restrictToSchema({
      email: required(isEmail),
      name: required(minLength(USER_NAME_MIN_LENGTH, checkType(String))),
      password: required(minLength(USER_PASSWORD_MIN_LENGTH)),
      password_confirmation: required(checkType(String)),
      type: required(isEnum(USER_TYPES))
    }),
    validateMatchingPasswords
  ])
);

module.exports = config => {
  return catchErrors(config, async (req, res) => {
    validateRegister(req.body, ["user"]);

    const { email, password, name, avatar, type } = req.body;
    const { user, token } = await config.service.register({
      email,
      password,
      name,
      avatar,
      type
    });

    res.status(CREATED).json({ user, token });
  });
};
