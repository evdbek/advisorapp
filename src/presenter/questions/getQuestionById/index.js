const catchErrors = require("../../utils/catchErrors");
const { OK } = require("http-status-codes");
const getAuthUser = require("../../../utils/jwt/getAuthUser");

module.exports = config => {
  return catchErrors(config, async (req, res) => {
    const { user } = await getAuthUser({ req, service: config.service });

    const { question_id } = req.params;

    const fetchedQuestion = await config.service.getQuestionById({
      id: question_id
    });

    res.status(OK).json(fetchedQuestion);
  });
};
