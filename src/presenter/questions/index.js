const createQuestion = require("./createQuestion");
const getQuestionById = require("./getQuestionById");
const getQuestions = require("./getQuestions");

module.exports = {
  createQuestion,
  getQuestions,
  getQuestionById
};
