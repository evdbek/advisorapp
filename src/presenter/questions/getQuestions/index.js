const catchErrors = require("../../utils/catchErrors");
const { OK } = require("http-status-codes");
const getAuthUser = require("../../../utils/jwt/getAuthUser");

module.exports = config => {
  return catchErrors(config, async (req, res) => {
    const { user } = await getAuthUser({ req, service: config.service });

    const fetchedQuestions = await config.service.getQuestions({
      user
    });

    res.status(OK).json(fetchedQuestions);
  });
};
