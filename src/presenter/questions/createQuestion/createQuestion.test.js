const initTests = require("../../utils/initTests");
const {
  API_ROUTE,
  USER_TYPE_CLIENT,
  USER_TYPE_USER
} = require("../../../utils/constants");
const expectError = require("../../utils/expectError");
const generateJwtToken = require("../../../utils/jwt/generateToken");
const createUser = require("../../utils/createUser");
const moment = require("moment");
const { fakeUsers } = require("../../../utils/fakesFactory");
const {
  CREATED,
  CONFLICT,
  UNAUTHORIZED,
  FORBIDDEN,
  UNPROCESSABLE_ENTITY,
  NOT_FOUND
} = require("http-status-codes");
const {
  TEST_INVALID_JWT_TOKEN,
  TEST_VALID_TITLE,
  TEST_VALID_DESCRIPTION
} = require("../../../utils/testValues");

describe(__filename, () => {
  const { service, request } = initTests();

  it("should fail to create question when unauthenticated", async () => {
    const response = await request.post(`${API_ROUTE}/questions`);
    expectError(response, UNAUTHORIZED);
  });

  it("should fail to create question when invalid token provided in authorization header", async () => {
    const response = await request
      .post(`${API_ROUTE}/questions`)
      .set("Authorization", TEST_INVALID_JWT_TOKEN);
    expectError(response, UNAUTHORIZED);
  });

  it("should fail to create question user_id is not provided", async () => {
    const client = await createUser(service, USER_TYPE_CLIENT);

    const validToken = await generateJwtToken({ id: client.id });
    const response = await request
      .post(`${API_ROUTE}/questions`)
      .set("Authorization", validToken)
      .send({
        title: TEST_VALID_TITLE,
        body: TEST_VALID_DESCRIPTION
      });
    expectError(response, UNPROCESSABLE_ENTITY);
  });

  it("should fail to create question when title is missing", async () => {
    const client = await createUser(service, USER_TYPE_CLIENT);
    const user = await createUser(service, USER_TYPE_USER);

    const validToken = await generateJwtToken({ id: client.id });
    const response = await request
      .post(`${API_ROUTE}/questions`)
      .set("Authorization", validToken)
      .send({
        user_id: user.id,
        body: TEST_VALID_DESCRIPTION
      });
    expectError(response, UNPROCESSABLE_ENTITY);
  });

  it("should fail to create question when body is missing", async () => {
    const client = await createUser(service, USER_TYPE_CLIENT);
    const user = await createUser(service, USER_TYPE_USER);

    const validToken = await generateJwtToken({ id: client.id });
    const response = await request
      .post(`${API_ROUTE}/questions`)
      .set("Authorization", validToken)
      .send({
        user_id: user.id,
        title: TEST_VALID_DESCRIPTION
      });
    expectError(response, UNPROCESSABLE_ENTITY);
  });

  it("should fail to create question when user_id does not exist", async () => {
    const client = await createUser(service, USER_TYPE_CLIENT);

    const validToken = await generateJwtToken({ id: client.id });
    const response = await request
      .post(`${API_ROUTE}/questions`)
      .set("Authorization", validToken)
      .send({
        user_id: "999",
        title: TEST_VALID_TITLE,
        body: TEST_VALID_DESCRIPTION
      });
    expectError(response, UNPROCESSABLE_ENTITY);
  });

  it("should successfully create question with valid data", async () => {
    const user = await createUser(service, USER_TYPE_USER);
    const client = await createUser(service, USER_TYPE_CLIENT);

    const validToken = await generateJwtToken({ id: client._id });
    const response = await request
      .post(`${API_ROUTE}/questions`)
      .set("Authorization", validToken)
      .send({
        user_id: user._id,
        title: TEST_VALID_TITLE,
        body: TEST_VALID_DESCRIPTION
      });
    expect(response.status).toBe(CREATED);

    const createdPost = response.body;
    const now = moment();
    const correctCreatedAt =
      moment.duration(now.diff(createdPost.created_at)).asMilliseconds() <
      10000;
    expect(createdPost.title).toEqual(TEST_VALID_TITLE);
    expect(createdPost.body).toEqual(TEST_VALID_DESCRIPTION);
    expect(correctCreatedAt).toBe(true);
  });
});
