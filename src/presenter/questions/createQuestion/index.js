const catchErrors = require("../../utils/catchErrors");
const { OK } = require("http-status-codes");
const getAuthUser = require("../../../utils/jwt/getAuthUser");
// prettier-ignore
const { isEmail, maxLength, minLength, isEnum, validateMatchingPasswords } = require("../../../utils/validate");
// prettier-ignore
const { maybe, required, checkType, restrictToSchema, composeRules} = require("rulr");
// prettier-ignore
const { QUESTIONS_ADD_CHANNEL, VARCHAR_FIELD_LENGTH, TEXT_FIELD_LENGTH, USER_TYPES } = require("../../../utils/constants");

const { CREATED } = require("http-status-codes");

const validateCreateQuestion = maybe(
  composeRules([
    restrictToSchema({
      user_id: required(checkType(String)),
      title: required(maxLength(VARCHAR_FIELD_LENGTH)),
      body: required(maxLength(TEXT_FIELD_LENGTH))
    })
  ])
);

module.exports = config => {
  return catchErrors(config, async (req, res) => {
    const { user } = await getAuthUser({ req, service: config.service });

    validateCreateQuestion(req.body, ["question"]);

    const createdQuestion = await config.service.createQuestion({
      client: user,
      ...req.body
    });

    if (config.pusher) {
      config.pusher.trigger(
        req.body.user_id,
        QUESTIONS_ADD_CHANNEL,
        createdQuestion
      );
    }

    res.status(CREATED).json(createdQuestion);
  });
};
