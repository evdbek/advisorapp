const facade = require("./facade");
const repoFactory = require("../repo/");
const repo = repoFactory();

module.exports = () =>
  facade({
    repo
  });
