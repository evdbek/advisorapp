module.exports = config => async ({ id }) => {
  return config.repo.getUserById({
    id
  });
};
