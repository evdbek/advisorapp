module.exports = config => async options => {
  return config.repo.getUsers(options);
};
