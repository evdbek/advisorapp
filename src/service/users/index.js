const createUser = require("./createUser");
const getUsers = require("./getUsers");
const getUserById = require("./getUserById");

module.exports = {
  createUser,
  getUsers,
  getUserById
};
