const { login, register } = require("./auth");
const { createUser, getUsers, getUserById } = require("./users");
const {
  createQuestion,
  getQuestionById,
  getQuestions
} = require("./questions");
const { createComment } = require("./comments");
const { migrate, rollback, clearService } = require("./utils");

module.exports = config => {
  return {
    login: login(config),
    register: register(config),

    createUser: createUser(config),
    getUsers: getUsers(config),
    getUserById: getUserById(config),

    createQuestion: createQuestion(config),
    getQuestions: getQuestions(config),
    getQuestionById: getQuestionById(config),

    createComment: createComment(config),

    clearService: clearService(config),
    migrate: migrate(config),
    rollback: rollback(config)
  };
};
