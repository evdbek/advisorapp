const { ModelNotFoundError } = require("../../../utils/errors");
module.exports = config => async options => {
  const question = await config.repo.getQuestionById({
    id: options.question_id
  });

  if (question === null) {
    throw new ModelNotFoundError("Question");
  }

  return config.repo.createComment({ question, ...options });
};
