const createQuestion = require("./createQuestion");
const getQuestions = require("./getQuestions");
const getQuestionById = require("./getQuestionById");

module.exports = {
  createQuestion,
  getQuestions,
  getQuestionById
};
