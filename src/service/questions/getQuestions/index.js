module.exports = config => async ({ user }) => {
  return config.repo.getQuestions({
    user
  });
};
