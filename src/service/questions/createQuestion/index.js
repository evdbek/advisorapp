const { ModelNotFoundError } = require("../../../utils/errors");
module.exports = config => async options => {
  const user = await config.repo.getUserById({ id: options.user_id });

  if (user === null || user.type == "client") {
    throw new ModelNotFoundError("user");
  }
  return config.repo.createQuestion({ user, ...options });
};
