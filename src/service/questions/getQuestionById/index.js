module.exports = config => async ({ id }) => {
  return config.repo.getQuestionById({
    id
  });
};
