const {
  ModelNotFoundError,
  InvalidCredentialsError
} = require("../../../utils/errors");
const verifyPassword = require("../../../utils/verifyPassword");
const generateToken = require("../../../utils/jwt/generateToken");

module.exports = config => {
  return async ({ email, password }) => {
    try {
      const user = await config.repo.getUserByEmail({ email });

      const match = await verifyPassword(password, user.password);

      if (!match) throw new InvalidCredentialsError();

      const token = await generateToken({
        id: user.id,
        name: user.name,
        email: user.email,
        avatar: user.avatar,
        type: user.type
      });

      return { user, token };
    } catch (err) {
      if (err instanceof ModelNotFoundError) {
        throw new InvalidCredentialsError();
      }
      throw err;
    }
  };
};
