const verifyPassword = require("../../../utils/verifyPassword");
const hashPassword = require("../../../utils/hashPassword");
const generateToken = require("../../../utils/jwt/generateToken");
const gravatar = require("gravatar");

module.exports = config => {
  return async options => {
    const password = await hashPassword(options.password);
    const avatar = gravatar.url(options.email, {
      s: "200", // Size
      r: "pg", // Rating
      d: "mm" // Default
    });

    user = await config.repo.createUser({
      name: options.name,
      email: options.email,
      type: options.type,
      avatar,
      password
    });

    const token = await generateToken({
      id: user.id,
      name: user.name,
      email: user.email,
      avatar: user.avatar,
      type: user.type
    });

    return { user, token };
  };
};
