const faker = require("faker");
const R = require("ramda");
const { USER_TYPES } = require("./constants");

const fakeUsers = options => {
  const settings = {
    count: 1,
    only: [],
    overrides: options.overrides,
    ...options
  };
  const items = [];
  for (let i = 0; i < settings.count; i++) {
    const password = faker.internet.password();
    const user = {
      id: i + 1,
      email: faker.internet.exampleEmail(),
      password,
      password_confirmation: password,
      name: faker.name.firstName(),
      ...settings.overrides
    };
    const pickable = settings.only.length ? settings.only : Object.keys(user);
    items.push(R.pick(pickable, user));
  }
  return items.length > 1 ? items : items[0];
};

const fakeQuestions = options => {
  const settings = {
    count: 1,
    only: [],
    overrides: {},
    ...options
  };
  const items = [];
  for (let i = 0; i < settings.count; i++) {
    const post = {
      id: i + 1,
      user_id: 1,
      client_id: 2,
      title: faker.lorem.sentences(1),
      body: faker.lorem.sentences(10),
      ...settings.overrides
    };
    const pickable = settings.only.length ? settings.only : Object.keys(post);
    items.push(R.pick(pickable, post));
  }
  return items.length > 1 ? items : items[0];
};

const fakeComments = options => {
  const settings = {
    count: 1,
    only: [],
    overrides: {},
    ...options
  };
  const items = [];
  for (let i = 0; i < settings.count; i++) {
    const post = {
      id: i + 1,
      user_id: 1,
      client_id: 2,
      post_id: 1,
      body: faker.lorem.sentences(10),
      ...settings.overrides
    };
    const pickable = settings.only.length ? settings.only : Object.keys(post);
    items.push(R.pick(pickable, post));
  }
  return items.length > 1 ? items : items[0];
};

module.exports = {
  fakeUsers,
  fakeQuestions,
  fakeComments
};
