const config = require("../../config/config");
const jwt = require("jsonwebtoken");
const { AUTH_SCHEME_NAME } = require("../constants");

module.exports = async payload => {
  return new Promise((resolve, reject) => {
    jwt.sign(
      payload,
      config.jwt.secret,
      { algorithm: config.jwt.algorithm, expiresIn: config.jwt.expiresIn },
      (err, token) => {
        if (err) reject(err);
        resolve(`${AUTH_SCHEME_NAME} ${token}`);
      }
    );
  });
};
