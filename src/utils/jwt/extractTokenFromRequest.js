const { AUTH_BODY_FIELD_NAME, AUTH_HEADER_NAME } = require("../constants");
const { MissingJwtTokenError } = require("../errors");

const createFromBodyFieldExtractor = options => req => {
  if (req.body && typeof req.body[options.fieldName] === "string") {
    return req.body[options.fieldName];
  }
  return null;
};

const createFromHeaderExtractor = options => req => {
  if (typeof req.headers[options.headerName] === "string") {
    return req.headers[options.headerName] || null;
  }
  return null;
};

const defaultConfig = {
  extractors: [
    createFromBodyFieldExtractor({ fieldName: AUTH_BODY_FIELD_NAME }),
    createFromHeaderExtractor({ headerName: AUTH_HEADER_NAME })
  ]
};

const createExtractTokenFromRequest = (config = defaultConfig) => options => {
  let token = config.extractors.reduce((accumulator, extractor) => {
    return accumulator || extractor(options.req) || null;
  }, null);

  if (token === null) throw new MissingJwtTokenError();
  if (token.startsWith("Bearer ")) {
    // Remove Bearer from string
    token = token.slice(7, token.length);
  }
  return token;
};

module.exports = {
  createExtractTokenFromRequest
};
