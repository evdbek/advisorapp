const jwt = require("jsonwebtoken");
const config = require("../../config/config");
const { createExtractTokenFromRequest } = require("./extractTokenFromRequest");
const extractTokenFromRequest = createExtractTokenFromRequest();
const {
  ExpiredJwtTokenError,
  InvalidJwtTokenError
} = require("../../utils/errors");

module.exports = async options => {
  try {
    const tokenExtractor =
      options.extractTokenFromRequest || extractTokenFromRequest;
    const token = tokenExtractor({ req: options.req });
    const secret = options.secretOrKey || config.jwt.secret;

    const payload = jwt.verify(token, secret);
    const user = await options.service.getUserById({ id: payload.id });

    return Promise.resolve({ user });
  } catch (err) {
    if (err instanceof jwt.JsonWebTokenError) {
      throw new InvalidJwtTokenError();
    } else if (err instanceof jwt.NotBeforeError) {
      new InvalidJwtTokenError();
    } else if (err instanceof jwt.TokenExpiredError) {
      new ExpiredJwtTokenError();
    }
    throw err;
  }
};
