module.exports = {
  API_ROUTE: "/api",

  USER_PASSWORD_MIN_LENGTH: 6,
  USER_TYPES: ["user", "client"],
  USER_TYPE_USER: "user",
  USER_TYPE_CLIENT: "client",
  USER_NAME_MIN_LENGTH: 2,

  DEFAULT_USERS_PAGINATION_LIMIT: 20,
  DEFAULT_QUESTIONS_ORDER: [["created_at", "desc"]],

  QUESTIONS_ADD_CHANNEL: "question-inserted",
  COMMENTS_ADD_CHANNEL: "comment-inserted",

  VARCHAR_FIELD_LENGTH: 255,
  TEXT_FIELD_LENGTH: 21844,

  AUTH_SCHEME_NAME: "Bearer",
  AUTH_BODY_FIELD_NAME: "auth_token",
  AUTH_HEADER_NAME: "authorization",

  ONE_HOUR: 36000,

  USER_MODEL_VISIBLE_PROPERTIES: [
    "_id",
    "name",
    "email",
    "password",
    "avatar",
    "email",
    "type",
    "created_at",
    "updated_at"
  ],
  QUESTION_MODEL_VISIBLE_PROPERTIES: [
    "_id",
    "user_id",
    "client_id",
    "title",
    "body",
    "created_at",
    "updated_at"
  ]
};
