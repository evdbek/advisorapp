const rulr = require("rulr");

const { Rule, Warning, Warnings, checkRegex, checkBool, Path } = rulr;

class InvalidEmailWarning extends Warning {
  constructor(data, path) {
    super(data, path);
  }
}

class InvalidUserTypeWarning extends Warning {
  constructor(data, path) {
    super(data, path);
  }
}

class MinLengthWarning extends Warning {
  constructor(data, path, length) {
    super(data, path);
    this.length = length;
  }
}

class MaxLengthWarning extends Warning {
  constructor(data, path, length) {
    super(data, path);
    this.length = length;
  }
}

class NotMatchingPasswordWarning extends Warning {
  constructor(data, path) {
    super(data, path);
  }
}

const createMinLengthWarning = (data, path, length) =>
  new MinLengthWarning(data, path, length);

const createMaxLengthWarning = (data, path, length) =>
  new MaxLengthWarning(data, path, length);

const createInvalidEmailWarning = (data, path) =>
  new InvalidEmailWarning(data.password, path);

const createInvalidUserTypeWarning = (data, path) =>
  new InvalidUserTypeWarning(data, path);

const createNotMatchingPasswordWarning = (data, path) =>
  new NotMatchingPasswordWarning(data.password, path);

const validateMatchingPasswords = (data, path) => {
  return data.password &&
    data.password_confirmation &&
    data.password === data.password_confirmation
    ? []
    : [createNotMatchingPasswordWarning(data, path)];
};

const minLength = (length, rule) => (data, path) => {
  return data.length >= length
    ? rule
      ? rule(data, path)
      : []
    : [createMinLengthWarning(data, path, length)];
};

const maxLength = (length, rule) => (data, path) => {
  return data.length <= length
    ? rule
      ? rule(data, path)
      : []
    : [createMaxLengthWarning(data, path, length)];
};

const isEmail = checkRegex(
  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  createInvalidEmailWarning
);

const isEnum = enums => (data, path) => {
  return enums.includes(data) ? [] : createInvalidUserTypeWarning(data, path);
};

module.exports = {
  InvalidEmailWarning,
  MinLengthWarning,
  MaxLengthWarning,
  NotMatchingPasswordWarning,
  createMinLengthWarning,
  createMaxLengthWarning,
  createMaxLengthWarning,
  createNotMatchingPasswordWarning,
  validateMatchingPasswords,
  minLength,
  maxLength,
  isEmail,
  isEnum
};
