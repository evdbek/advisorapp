const bcrypt = require("bcryptjs");
module.exports = async function(input) {
  return bcrypt.hash(input, 12);
};
