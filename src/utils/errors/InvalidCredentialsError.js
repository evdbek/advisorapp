const BaseError = require("./BaseError");

module.exports = class InvalidCredentialsError extends BaseError {};
