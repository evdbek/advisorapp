const BaseError = require("./BaseError");

module.exports = class ModelNotFoundError extends BaseError {
  constructor(modelName = "") {
    super();
    this.modelName = modelName;
  }
};
