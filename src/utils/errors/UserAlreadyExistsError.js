const BaseError = require("./BaseError");

module.exports = class UserAlreadyExistsError extends BaseError {};
