const BaseError = require("./BaseError");

module.exports = class MissingJwtTokenError extends BaseError {};
