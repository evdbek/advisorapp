const BaseError = require("./BaseError");
module.exports = class ForbiddenError extends BaseError {};
