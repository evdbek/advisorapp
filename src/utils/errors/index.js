const BaseError = require("./BaseError");
const ForbiddenError = require("./ForbiddenError");
const ExpiredJwtTokenError = require("./ExpiredJwtTokenError");
const InvalidJwtTokenError = require("./InvalidJwtTokenError");
const NotFoundError = require("./NotFoundError");
const UnauthorizedError = require("./UnauthorizedError");
const UserAlreadyExistsError = require("./UserAlreadyExistsError");
const InvalidCredentialsError = require("./InvalidCredentialsError");
const ModelNotFoundError = require("./ModelNotFoundError");
const MissingJwtTokenError = require("./MissingJwtTokenError");

module.exports = {
  BaseError,
  ForbiddenError,
  ExpiredJwtTokenError,
  InvalidJwtTokenError,
  NotFoundError,
  UnauthorizedError,
  UserAlreadyExistsError,
  InvalidCredentialsError,
  ModelNotFoundError,
  MissingJwtTokenError
};
