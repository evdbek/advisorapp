const BaseError = require("./BaseError");
module.exports = class ExpiredJwtTokenError extends BaseError {};
