const BaseError = require("./BaseError");

module.exports = class UnauthorizedError extends BaseError {};
