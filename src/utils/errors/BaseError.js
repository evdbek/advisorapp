module.exports = class BaseError {
  constructor(message = "Error") {
    this.message = message;
    this.name = this.constructor.name;
    this.stack = new Error(this.message).stack;
  }
};
