const BaseError = require("./BaseError");

module.exports = class NotFoundError extends BaseError {};
