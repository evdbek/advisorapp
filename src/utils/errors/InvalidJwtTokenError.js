const BaseError = require("./BaseError");

module.exports = class InvalidJwtTokenError extends BaseError {};
