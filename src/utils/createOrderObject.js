module.exports = (sortString, pickableAttributes = []) => {
  return sortString
    .toLowerCase()
    .split(",")
    .reduce((prev, curr) => {
      const [key, value] = curr.split(":");
      const item = [[key, value]];
      if (pickableAttributes.length) {
        return pickableAttributes.indexOf(key) !== -1
          ? [...prev, ...item]
          : prev;
      }
      return [...prev, ...item];
    }, []);
};
