const bcrypt = require("bcryptjs");

module.exports = async function(plainPassword, hashedPassword) {
  return bcrypt.compare(plainPassword, hashedPassword);
};
