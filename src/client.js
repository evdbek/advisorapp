const io = require("socket.io-client");

const socket = io.connect(
  "http://localhost:3001",
  {
    transports: ["websocket"],
    forceNew: true,
    reconnection: false
  }
);

socket.on("connect", () => {
  console.log("Successful connection to the server.");

  socket.send("Hello from the client");

  socket.on("message", msg => {
    console.log("Message from server: " + msg);
    socket.send(msg);
  });
});
