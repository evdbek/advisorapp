const createUser = require("../../modelRepo/users/createUser/mongoose");
const getUsers = require("../../modelRepo/users/getUsers/mongoose");
const getUserById = require("../../modelRepo/users/getUserById/mongoose");
const getUserByEmail = require("../../modelRepo/users/getUserByEmail/mongoose");

const createQuestion = require("../../modelRepo/questions/createQuestion/mongoose");
const getQuestions = require("../../modelRepo/questions/getQuestions/mongoose");
const getQuestionById = require("../../modelRepo/questions/getQuestionById/mongoose");

const createComment = require("../../modelRepo/comments/createComment/mongoose");

const migrate = require("../../modelRepo/commons/migrate/mongoose");
const rollback = require("../../modelRepo/commons/rollback/mongoose");
const clearRepo = require("../../modelRepo/commons/clearRepo/mongoose");

module.exports = config => {
  return {
    createUser: createUser(config),
    getUsers: getUsers(config),
    getUserById: getUserById(config),
    getUserByEmail: getUserByEmail(config),

    createQuestion: createQuestion(config),
    getQuestions: getQuestions(config),
    getQuestionById: getQuestionById(config),

    createComment: createComment(config),

    clearRepo: clearRepo(config),
    migrate: migrate(config),
    rollback: rollback(config)
  };
};
