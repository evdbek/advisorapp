const config = require("../config/config");
const facade = require("./facade");

module.exports = () =>
  facade({
    modelRepoName: config,
    mongoose: config.mongoose
  });
