const { models, mongoose } = require("../models");
const mongooseRepo = require("./utils/mongoose/");
const modelRepoFactory = name => {
  switch (name) {
    default:
    case "mongoose":
      return mongooseRepo({
        mongooseInstance: mongoose,
        models
      });
  }
};

module.exports = config => {
  return {
    ...modelRepoFactory(config.modelRepoName)
  };
};
