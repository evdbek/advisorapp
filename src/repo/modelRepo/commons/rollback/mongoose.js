module.exports = config => async () => {
  await Promise.resolve(() => {
    for (var collection in config.mongooseInstance.connection.collections) {
      config.mongooseInstance.connection.collections[collection].drop();
    }
    return true;
  });
};
