module.exports = config => async () => {
  (await config.mongooseInstance.connection.db) &&
    config.mongooseInstance.connection.db.dropDatabase();
};
