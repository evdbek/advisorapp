const { UserAlreadyExistsError } = require("../../../../utils/errors");

module.exports = config => {
  return async options => {
    try {
      const question = await config.models.Question.create({
        client: options.client,
        user: options.user,
        name: options.client.name,
        avatar: options.avatar,
        title: options.title,
        body: options.body
      });

      return question;
    } catch (err) {
      throw err;
    }
  };
};
