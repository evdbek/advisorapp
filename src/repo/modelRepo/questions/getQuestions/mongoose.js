const { ModelNotFoundError } = require("../../../../utils/errors/");
const { DEFAULT_QUESTIONS_ORDER } = require("../../../../utils/constants");
const createOrderObject = require("../../../../utils/createOrderObject");

module.exports = config => {
  return async options => {
    let questions = {};
    let filter = {};
    if (options.user.type == "client") filter = { client: options.user };
    else filter = { user: options.user };

    questions = await config.models.Question.find(filter)
      .populate("client", ["name", "avatar"])
      .sort(
        options.order
          ? createOrderObject(options.order)
          : DEFAULT_QUESTIONS_ORDER
      );

    return questions;
  };
};
