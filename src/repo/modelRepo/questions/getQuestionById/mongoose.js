const { ModelNotFoundError } = require("../../../../utils/errors/");

module.exports = config => {
  return async options => {
    if (!config.mongooseInstance.Types.ObjectId.isValid(options.id))
      throw new ModelNotFoundError("Question");

    const question = await config.models.Question.findById(options.id).populate(
      "client",
      ["name", "avatar"]
    );

    if (question == null) throw new ModelNotFoundError("Question");

    return question;
  };
};
