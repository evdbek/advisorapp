const { ModelNotFoundError } = require("../../../../utils/errors/");
const {
  USER_MODEL_VISIBLE_PROPERTIES
} = require("../../../../utils/constants");

module.exports = config => {
  return async options => {
    if (!config.mongooseInstance.Types.ObjectId.isValid(options.id))
      throw new ModelNotFoundError("User4");

    const user = await config.models.User.findById(options.id).select(
      USER_MODEL_VISIBLE_PROPERTIES
    );

    if (user == null) throw new ModelNotFoundError("User2");

    return user;
  };
};
