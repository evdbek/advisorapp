const { UserAlreadyExistsError } = require("../../../../utils/errors");

module.exports = config => {
  return async options => {
    try {
      const user = await config.models.User.create({
        name: options.name,
        email: options.email,
        avatar: options.avatar,
        password: options.password,
        type: options.type
      });
      return user;
    } catch (err) {
      if (err.name === "MongoError" && err.code === 11000) {
        throw new UserAlreadyExistsError();
      }
      throw err;
    }
  };
};
