const { ModelNotFoundError } = require("../../../../utils/errors/");
const {
  USER_MODEL_VISIBLE_PROPERTIES,
  DEFAULT_USERS_PAGINATION_LIMIT
} = require("../../../../utils/constants");

module.exports = config => {
  return async options => {
    const limit = options.limit
      ? options.limit
      : DEFAULT_USERS_PAGINATION_LIMIT;

    const filter = options.type ? { type: options.type } : {};
    const users = await config.models.User.find(filter)
      .select(USER_MODEL_VISIBLE_PROPERTIES)
      .limit(options.limit);

    return users;
  };
};
