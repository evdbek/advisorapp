const { ModelNotFoundError } = require("../../../../utils/errors/");
const {
  USER_MODEL_VISIBLE_PROPERTIES
} = require("../../../../utils/constants");

module.exports = config => {
  return async Options => {
    const user = await config.models.User.findOne({
      email: Options.email
    }).select(USER_MODEL_VISIBLE_PROPERTIES);

    if (user == null) throw new ModelNotFoundError("user");

    return user;
  };
};
