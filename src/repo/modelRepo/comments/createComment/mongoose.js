const { UserAlreadyExistsError } = require("../../../../utils/errors");

module.exports = config => {
  return async options => {
    try {
      options.question.comments.unshift({
        body: options.body,
        user: options.user,
        name: options.name,
        avatar: options.avatar
      });

      await options.question.save();

      return options.question;
    } catch (err) {
      throw err;
    }
  };
};
