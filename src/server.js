const express = require("express");
const app = express();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const presenter = require("./presenter");
const messengerFactory = require("./messenger");
const { API_ROUTE } = require("./utils/constants");
const Pusher = require("pusher");
const config = require("./config/config");
const path = require("path");
const serviceFactory = require("./service/index");

const service = serviceFactory();

const messenger = messengerFactory();

var pusher = new Pusher({
  appId: config.pusher.appId,
  key: config.pusher.key,
  secret: config.pusher.secret,
  cluster: config.pusher.cluster,
  encrypted: config.pusher.encrypted
});

app.use(API_ROUTE, presenter({ service, messenger, pusher }));

if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

const port = config.express.port;
http.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
