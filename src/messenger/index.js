const en = require("./languages").en;
const config = require("../config/config");

module.exports = () => {
  switch (config.lang) {
    case "en":
      return en;
  }
};
