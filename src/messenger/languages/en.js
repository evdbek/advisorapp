const stringPath = require("../utils/stringPath");

const Messenger = {
  // JWT
  expiredJwtToken: () => "Expired JWT token",
  invalidJwtToken: () => "Invalid JWT token",
  missingJwtToken: () => "Missing JWT token",

  modelNotFound: modelName => `No ${modelName} Model found`,

  // Generic
  unauthorized: () => "Unauthorized",
  forbidden: () => "Forbidden",
  serverError: () => "Server error",

  // Validation
  userAlreadyExists: () => "User already exists",
  invalidCredentials: () => "Incorrect email or password",

  // rulr Warnings
  invalidEmailWarning: warning => {
    const path = stringPath(warning.path);
    const dataString = JSON.stringify(warning.data);
    return `Invalid email in '${path}'. Received '${dataString}'`;
  },
  requiredWarning: warning => {
    const path = stringPath(warning.path);
    return `Missing required value in '${path}'`;
  },
  typeWarning: warning => {
    const path = stringPath(warning.path);
    const typeName = warning.type.name;
    const dataString = JSON.stringify(warning.data);
    return `Expected '${path}' to be '${typeName}'. Received '${dataString}'`;
  },
  restrictedKeysWarning: warning => {
    const path = stringPath(warning.path);
    const keys = warning.keys.join(", ");
    return `Unknown keys (${keys}) set in '${path}'`;
  },
  minLengthWarning: warning => {
    const pathString = stringPath(warning.path);
    const dataString = JSON.stringify(warning.data);
    return `Required data in ${pathString} must have at least ${
      warning.length
    } characters.`;
  },
  maxLengthWarning: warning => {
    const pathString = stringPath(warning.path);
    const dataString = JSON.stringify(warning.data);
    return `Required data in ${pathString} must have maximum ${
      warning.length
    } characters.`;
  },
  notMatchingPasswordWarning: warning => {
    const pathString = stringPath(warning.path);
    const dataString = JSON.stringify(warning.data);
    return `Passwords must match in ${pathString}`;
  },
  warning: warning => {
    const path = stringPath(warning.path);
    const dataString = JSON.stringify(warning.data);
    return `Problem in '${path}'. Received '${dataString}'`;
  }
};

module.exports = Messenger;
