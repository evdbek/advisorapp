const { ONE_HOUR } = require("../utils/constants");

module.exports = {
  lang: process.env.LANG || "en",
  express: {
    port: process.env.NODE_PORT || 3001
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    expiresIn: process.env.JWT_EXPIRES_IN || ONE_HOUR,
    algorithm: process.env.JWT_ALGORITHM || "HS256"
  },
  modelRepo: {
    name: process.env.MODEL_REPO_NAME || "mongoose"
  },
  mongoose: {
    development: {
      uri: process.env.DEV_DB_URI
    },
    production: {
      uri: process.env.DEV_DB_URI
    },
    test: {
      uri: process.env.DEV_DB_URI
    }
  },
  pusher: {
    appId: process.env.PUSHER_APP_ID,
    key: process.env.PUSHER_KEY,
    secret: process.env.PUSHER_SECRET,
    cluster: process.env.PUSHER_CLUSTER,
    encrypted: process.env.PUSHER_ENCRYPTED || true
  }
};
