export default {
  API_URL: "http://localhost:9000/api/",
  PUSHER_APP_KEY: process.env.PUSHER_APP_KEY || "fb58c2ce278b947edd6b",
  PUSHER_APP_CLUSTER: process.env.PUSHER_APP_CLUSTER || "eu"
};
