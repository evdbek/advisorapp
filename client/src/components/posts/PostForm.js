import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import TextAreaFieldGroup from "../common/TextAreaFieldGroup";
import TextFieldGroup from "../common/TextFieldGroup";
import SelectListGroup from "../common/SelectListGroup";
import { addPost, receivedPost } from "../../actions/postActions";
import config from "../../config";
import Pusher from "pusher-js";
import { toast } from "react-toastify";
class PostForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      advisor: {},
      title: "",
      body: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    this.pusher = new Pusher(config.PUSHER_APP_KEY, {
      cluster: config.PUSHER_APP_CLUSTER,
      encrypted: true
    });

    this.channel = this.pusher.subscribe(this.props.auth.user.id);
    this.channel.bind("question-inserted", data => {
      this.props.receivedPost(data);
      toast("New question '" + data.title + "' \n asked by " + data.client.name);
    });
  }

  componentWillReceiveProps(newProps) {
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const newPost = {
      body: this.state.body,
      title: this.state.title,
      user_id: this.state.advisor
      // avatar: user.avatar
    };

    this.props.addPost(newPost);
    this.setState({ title: "", body: "" });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;
    const { user } = this.props.auth;
    const { users } = this.props.post;
    var options = [];
    if (users) {
      options = users.map(function(advisor) {
        return { value: advisor._id, label: advisor.name };
      });
    }
    options.unshift({ value: 0, label: "* Choose an advisor" });

    return user.type === "client" ? (
      <div className="post-form mb-3">
        <div className="card card-info">
          <div className="card-header bg-info text-white">Ask Somthing...</div>
          <div className="card-body">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <SelectListGroup
                  placeholder="Choose an Advisor"
                  name="advisor"
                  value={this.state.advisor}
                  onChange={this.onChange}
                  options={options}
                  error={errors.status}
                  info="Choose an advisor to answer your question"
                />
                <TextFieldGroup
                  placeholder="* Question Title"
                  name="title"
                  value={this.state.title}
                  onChange={this.onChange}
                  error={errors.title}
                  info="Describe the subject of the question"
                />
                <TextAreaFieldGroup
                  placeholder="Create a Question"
                  name="body"
                  value={this.state.body}
                  onChange={this.onChange}
                  error={errors.body}
                />
              </div>
              <button type="submit" className="btn btn-dark">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    ) : null;
  }
}

PostForm.propTypes = {
  addPost: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors,
  post: state.post
});

export default connect(
  mapStateToProps,
  { addPost, receivedPost }
)(PostForm);
