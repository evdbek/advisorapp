## React and Node.js Web Application

This is a sample project is a web applicaton for trading messages.The backend is with node.js API and a fronend is built with React. 
A user can select to register as a User or as an Advisor. Users can ask advisors questions and Advisors can leave comments to each question.


## Running the Application

It's very simple to get the App up and running.
First configure the mLab db authentication by providinga valid uri

```
process.env.DEV_DB_URI=mongodb://<dbuser>:<dbpassword>@ds151513.mlab.com:51513/<databaseName>
```

Furthermore, in order to receive realtime updates you need also to configure Pusher credentials

```
process.env.PUSHER_APP_ID
process.env.PUSHER_KEY
process.env.PUSHER_SECRET
process.env.PUSHER_CLUSTER
```

In order to run in a development environment:

1. `npm install`
2. `npm run client-install `
3. `npm run dev` 

In order to run in a development environment:

1. `npm install`
2. `npm run client-install `
3. `npm run build --prefix client`
4. `npm run start`


In order to test, run:

```
npm run test
```